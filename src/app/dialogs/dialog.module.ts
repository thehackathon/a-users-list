import { NgModule } from '@angular/core';

import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  imports: [
    MatDialogModule,
    MatButtonModule,
  ],
  exports: [],
  declarations: [ConfirmationDialogComponent],
  providers: [],
})
export class DialogModule { }
