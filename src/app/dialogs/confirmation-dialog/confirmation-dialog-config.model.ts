export class ConfirmationDialogConfig {
  Title: string;
  Content: string;
  ButtonText: string;
  ButtonColor: string;
}
