import { Observable } from 'rxjs';

export interface ICRUD<Entity> {
  get(id: number): Observable<Entity>;
  getAll(): Observable<Entity[]>;
  create(entity: Entity): Observable<number>;
  update(entity: Entity): Observable<boolean>;
  delete(ids: number[]): Observable<boolean>;
}
