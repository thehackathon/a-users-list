export enum CRUDAction {
  Create = 1,
  Read,
  Update,
  Delete
}
