import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HeaderMenuComponent } from './components/header-menu.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [
    RouterModule,
    FontAwesomeModule,

    MatToolbarModule,
    MatIconModule
  ],
  exports: [
    HeaderMenuComponent,
  ],
  declarations: [
    HeaderMenuComponent,
  ],
  providers: [],
})
export class HeaderModule { }
