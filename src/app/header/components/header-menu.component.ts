import { Component, OnInit } from '@angular/core';
import { faUsers, faTicketAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header-menu',
  templateUrl: 'header-menu.component.html',
  styleUrls: ['header-menu.component.scss'],
})

export class HeaderMenuComponent implements OnInit {
  icons = {
    faUsers,
    faTicketAlt
  };

  constructor() { }

  ngOnInit() { }
}
