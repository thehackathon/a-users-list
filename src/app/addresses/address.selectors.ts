import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AddressesState } from './address.reducers';
import { Address } from './models/address.model';

export const selectAddressState = createFeatureSelector<AddressesState>('addresses');

export const selectAddressById = (id: number) => createSelector(
  selectAddressState,
  addressState => addressState.entities[id]
);

export const selectError = () => createSelector(
  selectAddressState,
  addressState => addressState.error
);

export const selectAddressesInStore = createSelector(
  selectAddressState,
  addressState => {
    const items: Address[] = [];
    for(let key in addressState.entities) {
      let address = addressState.entities[key];
      items.push(address);
    }
    return items;
  }
);
