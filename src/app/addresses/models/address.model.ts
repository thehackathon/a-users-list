import { Country } from './country.model';

export class Address {
  Id: number;
  Country: Country = new Country();
  Line1: string; // street, house, apartment
  Line2: string; // apartment, block (non-required)
  City: string;
  Zipcode: number;
}

