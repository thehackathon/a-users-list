import { Address } from './models/address.model';
import { Action } from "@ngrx/store";

export enum AddressesActionTypes {
  AddressesGridListRequested = "[Address Grid] List Requested",
  AddressesGridListLoaded = "[Address Grid] List Loaded",
  AddressRequested = "[Address] Address Requested",
  AddressLoaded = "[Address] Address Loaded",
  AddressLoadError = "[Address] Address Load Error",
}

export class AddressesGridListRequested implements Action {
  readonly type = AddressesActionTypes.AddressesGridListRequested;
  constructor() {}
}

export class AddressesGridListLoaded implements Action {
  readonly type = AddressesActionTypes.AddressesGridListLoaded;
  constructor(public payload: { addresses: Address[]; }) {}
}

export class AddressRequested implements Action {
  readonly type = AddressesActionTypes.AddressRequested;
  constructor(public payload: { id: number }) {}
}

export class AddressLoaded implements Action {
  readonly type = AddressesActionTypes.AddressLoaded;
  constructor(public payload: { address: Address }) {}
}

export class AddressLoadError implements Action {
  readonly type = AddressesActionTypes.AddressLoadError;
  constructor(public payload: { error: string }) {}
}

export type AddressesActions =
  | AddressesGridListRequested
  | AddressesGridListLoaded
  | AddressRequested
  | AddressLoaded
  | AddressLoadError;
