import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { AddressLocalService } from './address.service';
import { AppState } from 'src/app/reducers';
import {
  AddressesGridListRequested,
  AddressesActionTypes,
  AddressesGridListLoaded,
  AddressRequested,
  AddressLoaded,
  AddressLoadError,
} from './address.actions';
import {mergeMap, map, concatMap, withLatestFrom, filter, catchError} from 'rxjs/operators';
import { of, forkJoin } from 'rxjs';
import { selectAddressById } from './address.selectors';
import { Address } from './models/address.model';

@Injectable()
export class AddressesEffects {
  @Effect()
  loadAddressesGrid$ = this.actions$.pipe(
    ofType<AddressesGridListRequested>(AddressesActionTypes.AddressesGridListRequested),
    mergeMap(() => {
      const requestToServer = this.addressesService.getAll();
      return requestToServer;
    }),
    map(addresses => {
      return new AddressesGridListLoaded({ addresses: addresses });
    })
  );


  @Effect()
  loadAddress$ = this.actions$.pipe(
    ofType<AddressRequested>(AddressesActionTypes.AddressRequested),
    concatMap(({ payload }) => of(payload).pipe(
      withLatestFrom(this.store.pipe(select(selectAddressById(payload.id))))
    )),
    filter(([, address]) => {
      return !address;
    }),
    mergeMap(([{ id }]) => {
      return this.addressesService.get(id);
    }),
    catchError((error) => {
      return of(error);
    }),
    map((addressOrError: Address | string) => {
      let l = addressOrError instanceof Address;
      if(addressOrError instanceof Address) {
        return new AddressLoaded({ address: addressOrError });
      }
      return new AddressLoadError({error: addressOrError});
    }),
  );

  constructor(private actions$: Actions, private addressesService: AddressLocalService, private store: Store<AppState>) { }
}
