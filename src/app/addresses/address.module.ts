import { NgModule } from '@angular/core';

import { AddressLocalService } from './address.service'

import { StoreModule } from "@ngrx/store";
import { addressesReducer } from "./address.reducers";
import { EffectsModule } from "@ngrx/effects";
import { AddressesEffects } from "./address.effects";

@NgModule({
  imports: [
    StoreModule.forFeature('addresses', addressesReducer),
    EffectsModule.forFeature([AddressesEffects]),
  ],
  exports: [],
  declarations: [],
  providers: [AddressLocalService],
})
export class AddressModule { }
