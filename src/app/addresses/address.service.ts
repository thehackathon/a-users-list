import { ICRUD } from '../models/ICRUD';
import { Injectable } from '@angular/core';
import { Observable, of, throwError  } from 'rxjs';
import { Address } from './models/address.model';

@Injectable()
export class AddressLocalService implements ICRUD<Address> {
  private readonly ADDRESSES_KEY: string = 'ADDRESS';

  constructor() {
  }

  get(id: number): Observable<Address> {
    let address = this.getAddresses().filter(u => u.Id === id).pop();
    if (address) {
      return of(address);
    } else {
      return throwError(`Address with id ${id} does not exists.`);
    }
  }
  getAll(): Observable<Address[]> {
    return of(this.getAddresses());
  }
  create(entity: Address): Observable<number> {
    const addresses = this.getAddresses();
    let maxId = 0;
    if(addresses.length) {
      maxId = +(Math.max.apply(Math, addresses.map(u => u.Id)));
    }
    entity.Id = ++maxId;
    addresses.push(entity);
    this.saveAddresses(addresses);
    return of(entity.Id);
  }
  update(entity: Address): Observable<boolean> {
    const addresses = this.getAddresses().filter(u => u.Id !== entity.Id);
    addresses.push(entity);
    this.saveAddresses(addresses);
    return of(true);
  }
  delete(ids: number[]): Observable<boolean> {
    const addresses = this.getAddresses().filter(u => ids.every(id => id !== u.Id));
    this.saveAddresses(addresses);
    return of(true);
  }

  private getAddresses(): Address[] {
    const addressesJson = localStorage.getItem(this.ADDRESSES_KEY);
    const addressParsed = JSON.parse(addressesJson);
    const addresses: Address[] = [];
    if(Array.isArray(addressParsed)){
      addressParsed.forEach(address => {
        addresses.push(Object.assign(new Address(), address));
      });
    }
    return addresses;
  }

  private saveAddresses(addresses: Address[]): void {
    localStorage.setItem(this.ADDRESSES_KEY, JSON.stringify(addresses));
  }
}
