import { Address } from './models/address.model';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {AddressesActions, AddressesActionTypes} from './address.actions';

export interface AddressesState extends EntityState<Address> {
  error: string
}

export const adapter: EntityAdapter<Address> = createEntityAdapter<Address>({
  selectId: (address: Address) => address.Id
});

export const initialAddressesState: AddressesState = adapter.getInitialState({error: null});

export function addressesReducer(state = initialAddressesState, action: AddressesActions): AddressesState {
  switch (action.type) {
    default: return state;
    case AddressesActionTypes.AddressLoaded: {
      return adapter.addMany([action.payload.address], {
        ...initialAddressesState
      });
    }
    case AddressesActionTypes.AddressesGridListLoaded:
      return adapter.addMany(action.payload.addresses, {
        ...initialAddressesState
      });
    case AddressesActionTypes.AddressLoadError:
      return {
        ...state,
        error: action.payload.error
      };
  }
}
