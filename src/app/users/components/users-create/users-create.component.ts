import { AddressLocalService } from './../../../addresses/address.service';
import { CRUDAction } from 'src/app/models/crud-action.enum';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user.model';
import { UsersLocalService } from '../../users.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { appConfig } from 'src/app/app.constants';
import { Subscription, zip } from 'rxjs';
import { flatMap, switchMap } from 'rxjs/operators';
import { Address } from 'src/app/addresses/models/address.model';

@Component({
  selector: 'app-users-create',
  templateUrl: 'users-create.component.html',
  styleUrls: ['users-create.component.scss'],
})

export class UsersCreateComponent implements OnInit, OnDestroy {
  user: User;
  action: CRUDAction;
  subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UsersLocalService,
    private addressService: AddressLocalService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.action = this.route.snapshot.data.action;
    this.user = new User();
    this.user.DeliveryAddress = new Address();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  create(): void {
    // TODO: validation
    this.subscriptions.push(
      this.addressService.create(this.user.DeliveryAddress).pipe(
        switchMap((addressId: number) => {
          this.user.DeliveryAddressId = addressId;
          const userId$ = this.userService.create(this.user);
          return userId$;
        })
      ).subscribe(userId => {
        if(userId) {
          this.snackBar.open('User created', null, {
            duration: appConfig.NOTIFICATION_OUT_TIME,
          });
          this.router.navigate(['/users']);
        }
      })
    );

  }
}
