import { switchMap } from 'rxjs/operators';
import { selectUserById, selectError } from './../../users.selectors';
import { UsersLocalService } from './../../users.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CRUDAction } from 'src/app/models/crud-action.enum';
import { User } from '../../models/user.model';

import { ConfirmationDialogConfig } from 'src/app/dialogs/confirmation-dialog/confirmation-dialog-config.model';
import { appConfig } from 'src/app/app.constants';

import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation-dialog/confirmation-dialog.component';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import { UserRequested } from '../../users.actions';
import { AddressLocalService } from 'src/app/addresses/address.service';

@Component({
  selector: 'app-users-view',
  templateUrl: 'users-view.component.html',
  styleUrls: ['users-view.component.scss'],
})

export class UsersViewComponent implements OnInit, OnDestroy {
  action: CRUDAction;
  user: User;
  subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UsersLocalService,
    private addressService: AddressLocalService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.action = this.route.snapshot.data.action;
    let userId = +this.route.snapshot.params.id;

    this.store.dispatch(new UserRequested({id:userId}));
    this.subscriptions.push(
      this.store.pipe(select(selectUserById(userId))).subscribe((user: User) => this.user = user),
      this.store.pipe(select(selectError())).subscribe((error: string) => {
        if(error){
          this.snackBar.open(error, null, {
            duration: appConfig.NOTIFICATION_OUT_TIME,
          });
          this.router.navigate(['/users']);
        }
      })
    );

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  edit(): void {
    this.router.navigate([`/users/${this.user.Id}/edit`]);
  }

  delete(): void {
    const config = new MatDialogConfig<ConfirmationDialogConfig>();
    config.data = {
      Title: `Delete user ${this.user.Username}?`,
      Content: `This will delete user and cannot be undone.`,
      ButtonText: 'Delete',
      ButtonColor: 'warn',
    };
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, config);
    dialogRef.afterClosed().subscribe((result: string) => {
      if (Boolean(result)) {
        this.subscriptions.push(
          this.addressService.delete([this.user.DeliveryAddress.Id]).pipe(
            switchMap((addressDeleted: boolean) => {
              const userDeleted$ = this.userService.delete([this.user.Id]);
              return userDeleted$;
            })
          ).subscribe((userDeleted: boolean) => {
            if(userDeleted) {
              this.snackBar.open(`User ${this.user.Username} has been deleted`, null, {
                duration: appConfig.NOTIFICATION_OUT_TIME,
              });
              this.router.navigate([`/users`]);
            }
          })
        );
      }
    });
  }
}
