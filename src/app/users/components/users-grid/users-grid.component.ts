import { AddressLocalService } from './../../../addresses/address.service';
import { UsersGridListRequested } from './../../users.actions';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { ConfirmationDialogComponent } from '../../../dialogs/confirmation-dialog/confirmation-dialog.component';
import { User } from 'src/app/users/models/user.model';
import { ConfirmationDialogConfig } from 'src/app/dialogs/confirmation-dialog/confirmation-dialog-config.model';
import { appConfig } from 'src/app/app.constants';

import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsersLocalService } from '../../users.service';
import { Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { selectUsersInStore } from '../../users.selectors';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-users-grid',
  templateUrl: 'users-grid.component.html',
  styleUrls: ['users-grid.component.scss']
})

export class UsersGridComponent implements OnInit, OnDestroy {
  users: User[] = [];
  displayedColumns = ['Select', 'Id', 'Username', 'Email', 'Address', 'Actions'];
  selection = new SelectionModel<User>(true, []);
  subscriptions: Subscription[] = [];

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar,
    private userService: UsersLocalService,
    private addressService: AddressLocalService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.store.pipe(select(selectUsersInStore)).subscribe((users: User[]) => this.users = users)
    )
    this.getUsers();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  getUsers() {
    this.store.dispatch(new UsersGridListRequested());
  }

  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.users.length;
    return numSelected === numRows;
  }

  isAnySelected(): boolean {
    const numSelected = this.selection.selected.length;
    return numSelected > 0;
  }

  masterToggle(): void {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.users.forEach(row => this.selection.select(row));
    }
  }

  deleteUsers(usersToDelete: User[]): void {
    const usersString = usersToDelete.map(u => u.Username).join(', ');
    const singleUser = usersToDelete.length === 1;
    const config = new MatDialogConfig<ConfirmationDialogConfig>();
    config.data = {
      Title: `Delete user${singleUser ? '' : 's'} ${usersString}?`,
      Content: `This will delete user${singleUser ? '' : 's'} and cannot be undone.`,
      ButtonText: 'Delete',
      ButtonColor: 'warn',
    };
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, config);

    dialogRef.afterClosed().subscribe((result: string) => {
      // TODO: decompose
      if (Boolean(result)) {
        this.subscriptions.push(
          this.addressService.delete(usersToDelete.map(u => u.DeliveryAddress.Id)).pipe(
            switchMap((addressDeleted: boolean) => {
              const userDeleted$ = this.userService.delete(usersToDelete.map(u => u.Id));
              return userDeleted$;
            })
          ).subscribe((userDeleted: boolean) => {
            if(userDeleted) {
              if (singleUser) {
                this.openSnackBar(`User ${usersString} has been deleted`);
              } else {
                this.openSnackBar(`Users ${usersString} have been deleted`);
              }
              this.getUsers();
            }
          })
        );
      }
    });
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, null, {
      duration: appConfig.NOTIFICATION_OUT_TIME,
    });
  }

  view(userId: number): void {
    this.router.navigate([`/users/${userId}`]);
  }

  edit(userId: number): void {
    this.router.navigate([`/users/${userId}/edit`]);
  }

  create(): void {
    this.router.navigate(['/users/create']);
  }
}
