import { Address } from '../../../addresses/models/address.model';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-users-address-form',
  templateUrl: 'users-address-form.component.html',
  styleUrls: ['users-address-form.component.scss'],
})

export class UsersAddressFormComponent implements OnInit {
  @Input()
  model: Address;
  @Input()
  isFormReadOnly: boolean;

  constructor(
  ) { }

  ngOnInit() {
   }
}
