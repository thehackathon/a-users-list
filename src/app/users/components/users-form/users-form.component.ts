import { CRUDAction } from '../../../models/crud-action.enum';
import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { User } from 'src/app/users/models/user.model';

@Component({
  selector: 'app-users-form',
  templateUrl: 'users-form.component.html',
  styleUrls: ['users-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UsersFormComponent implements OnInit, OnChanges {
  @Input()
  action: CRUDAction;
  @Input()
  user: User;

  isFormReadOnly: boolean;
  writeActions: CRUDAction[] = [CRUDAction.Create, CRUDAction.Update];

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.action) {
      this.isFormReadOnly = !this.writeActions.some(a => a === this.action);
    }
  }
}
