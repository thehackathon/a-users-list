import { AddressLocalService } from './../../../addresses/address.service';
import { Store, select } from '@ngrx/store';
import { CRUDAction } from 'src/app/models/crud-action.enum';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user.model';
import { UsersLocalService } from '../../users.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { appConfig } from 'src/app/app.constants';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/reducers';
import { UserRequested } from '../../users.actions';
import { selectUserById, selectError } from '../../users.selectors';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-users-create',
  templateUrl: 'users-edit.component.html',
  styleUrls: ['users-edit.component.scss'],
})

export class UsersEditComponent implements OnInit, OnDestroy {
  user: User = new User();
  action: CRUDAction;
  subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UsersLocalService,
    private addressService: AddressLocalService,
    private snackBar: MatSnackBar,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.action = this.route.snapshot.data.action;
    let userId = +this.route.snapshot.params.id;
    this.store.dispatch(new UserRequested({id:userId}));
      this.subscriptions.push(
        this.store.pipe(select(selectUserById(userId))).subscribe((user: User) => Object.assign(this.user, user)),
        this.store.pipe(select(selectError())).subscribe((error: string) => {
          if(error){
            this.snackBar.open(error, null, {
              duration: appConfig.NOTIFICATION_OUT_TIME,
            });
            this.router.navigate(['/users']);
          }
        })
      );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  save(): void {
    // TODO: validation
    this.subscriptions.push(
      this.addressService.update(this.user.DeliveryAddress).pipe(
        switchMap((addressUpdated: boolean) => {
          const userUpdated$ = this.userService.update(this.user);
          return userUpdated$;
        })
      ).subscribe(userUpdated => {
        if(userUpdated) {
          this.snackBar.open('User updated', null, {
            duration: appConfig.NOTIFICATION_OUT_TIME,
          });
          this.router.navigate(['/users']);
        }
      })
    );
  }
}
