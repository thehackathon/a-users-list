import { ICRUD } from '../models/ICRUD';
import { Injectable } from '@angular/core';
import { User } from './models/user.model';
import { Observable, of, throwError  } from 'rxjs';
import { Address } from '../addresses/models/address.model';

@Injectable()
export class UsersLocalService implements ICRUD<User> {
  private readonly USERS_KEY: string = 'USERS';

  constructor() {
  }

  get(id: number): Observable<User> {
    let user = this.getUsers().filter(u => u.Id === id).pop();
    if (user) {
      if(!user.DeliveryAddress){
        user.DeliveryAddress= new Address();
      }
      return of(user);
    } else {
      return throwError(`User with id ${id} does not exists.`);
    }
  }
  getAll(): Observable<User[]> {
    return of(this.getUsers());
  }
  create(entity: User): Observable<number> {
    entity.DeliveryAddress = null;
    const users = this.getUsers();
    let maxId = 0;
    if(users.length) {
      maxId = +(Math.max.apply(Math, users.map(u => u.Id)));
    }
    entity.Id = ++maxId;
    users.push(entity);
    this.saveUsers(users);
    return of(entity.Id);
  }
  update(entity: User): Observable<boolean> {
    entity.DeliveryAddress = null;
    const users = this.getUsers().filter(u => u.Id !== entity.Id);
    users.push(entity);
    this.saveUsers(users);
    return of(true);
  }
  delete(ids: number[]): Observable<boolean> {
    const users = this.getUsers().filter(u => ids.every(id => id !== u.Id));
    this.saveUsers(users);
    return of(true);
  }

  private getUsers(): User[] {
    const usersJson = localStorage.getItem(this.USERS_KEY);
    const usersParsed = JSON.parse(usersJson);
    const users: User[] = [];
    if(Array.isArray(usersParsed)){
      usersParsed.forEach(user => {
        users.push(Object.assign(new User(), user));
      });
    }
    return users;
  }

  private saveUsers(users: User[]): void {
    localStorage.setItem(this.USERS_KEY, JSON.stringify(users));
  }
}
