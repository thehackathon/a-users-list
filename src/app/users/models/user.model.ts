import { Address } from '../../addresses/models/address.model';

export class User {
  Id: number;
  Username: string;
  Email: string;
  DeliveryAddress: Address;
  DeliveryAddressId: number;
}
