import { UsersEditComponent } from './components/users-edit/users-edit.component';
import { UsersCreateComponent } from './components/users-create/users-create.component';
import { CRUDAction } from './../models/crud-action.enum';
import { UsersGridComponent } from './components/users-grid/users-grid.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersViewComponent } from './components/users-view/users-view.component';

const routes: Routes = [
  {
    path: '',
    component: UsersGridComponent,
    pathMatch: 'full',
    data: { animation: 'Grid', }
  },
  {
    path: 'create',
    component: UsersCreateComponent,
    data: { action: CRUDAction.Create, animation: 'User' },
  },
  {
    path: ':id',
    children: [
      {
        path: '',
        component: UsersViewComponent,
        data: { action: CRUDAction.Read, animation: 'User' },
        pathMatch: 'full',
      },
      {
        path: 'edit',
        component: UsersEditComponent,
        data: { action: CRUDAction.Update, animation: 'User' },
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
