import { AddressModule } from './../addresses/address.module';
import { UsersViewComponent } from './components/users-view/users-view.component';
import { NgModule } from '@angular/core';

import { UsersRoutingModule } from './users-routing.module';
import { UsersFormComponent } from './components/users-form/users-form.component';
import { UsersGridComponent } from './components/users-grid/users-grid.component';
import { UsersCreateComponent } from './components/users-create/users-create.component';
import { UsersEditComponent } from './components/users-edit/users-edit.component';
import { UsersLocalService } from './users.service'
import { UsersAddressFormComponent } from './components/users-address-form/users-address-form.component';
import { DialogModule } from '../dialogs/dialog.module';
// material
import { MatTableModule, } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCommonModule, MatPseudoCheckboxModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';

import { StoreModule } from "@ngrx/store";
import { usersReducer } from "./users.reducers";
import { EffectsModule } from "@ngrx/effects";
import { UsersEffects } from "./users.effects";

@NgModule({
  imports: [
    UsersRoutingModule,
    DialogModule,
    CommonModule,

    MatTableModule,
    MatSortModule,
    MatCommonModule,
    MatPaginatorModule,
    MatPseudoCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatTooltipModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatMenuModule,
    MatCardModule,
    MatInputModule,

    AddressModule,

    StoreModule.forFeature('users', usersReducer),
    EffectsModule.forFeature([UsersEffects]),
  ],
  exports: [],
  declarations: [
    UsersGridComponent,
    UsersFormComponent,
    UsersAddressFormComponent,
    UsersViewComponent,
    UsersCreateComponent,
    UsersEditComponent,
  ],
  providers: [UsersLocalService],
})
export class UsersModule { }
