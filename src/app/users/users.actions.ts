import { Action } from "@ngrx/store";
import { User } from "./models/user.model";

export enum UsersActionTypes {
  UsersGridListRequested = "[User Grid] List Requested",
  UsersGridListLoaded = "[User Grid] List Loaded",
  UserRequested = "[User] User Requested",
  UserLoaded = "[User] User Loaded",
  UserLoadError = "[User] User Load Error",
}

export class UsersGridListRequested implements Action {
  readonly type = UsersActionTypes.UsersGridListRequested;
  constructor() {}
}

export class UsersGridListLoaded implements Action {
  readonly type = UsersActionTypes.UsersGridListLoaded;
  constructor(public payload: { users: User[]; }) {}
}

export class UserRequested implements Action {
  readonly type = UsersActionTypes.UserRequested;
  constructor(public payload: { id: number }) {}
}

export class UserLoaded implements Action {
  readonly type = UsersActionTypes.UserLoaded;
  constructor(public payload: { user: User }) {}
}

export class UserLoadError implements Action {
  readonly type = UsersActionTypes.UserLoadError;
  constructor(public payload: { error: string }) {}
}

export type UsersActions =
  | UsersGridListRequested
  | UsersGridListLoaded
  | UserRequested
  | UserLoaded
  | UserLoadError;
