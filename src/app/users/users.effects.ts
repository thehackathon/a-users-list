import { AddressRequested } from './../addresses/address.actions';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { UsersLocalService } from './users.service';
import { AppState } from 'src/app/reducers';
import {
  UsersGridListRequested,
  UsersActionTypes,
  UsersGridListLoaded,
  UserRequested,
  UserLoaded,
  UserLoadError,
} from './users.actions';
import {mergeMap, map, concatMap, withLatestFrom, filter, catchError, tap} from 'rxjs/operators';
import { of, forkJoin } from 'rxjs';
import { selectUserById } from './users.selectors';
import { User } from './models/user.model';
import { AddressesGridListRequested } from '../addresses/address.actions';

@Injectable()
export class UsersEffects {
  @Effect()
  loadUsersGrid$ = this.actions$.pipe(
    ofType<UsersGridListRequested>(UsersActionTypes.UsersGridListRequested),
    tap(() => this.store.dispatch(new AddressesGridListRequested())),
    mergeMap(() => this.userService.getAll()),
    map(users => new UsersGridListLoaded({ users: users })),
  );


  @Effect()
  loadUser$ = this.actions$.pipe(
    ofType<UserRequested>(UsersActionTypes.UserRequested),
    concatMap(({ payload }) => of(payload).pipe(
      withLatestFrom(this.store.pipe(select(selectUserById(payload.id))))
    )),
    tap(([, user]) => user && this.store.dispatch(new AddressRequested({ id: user.DeliveryAddressId }))),
    filter(([, user]) => {
      return !user;
    }),
    mergeMap(([{ id }]) => {
      return this.userService.get(id);
    }),
    tap((user) => user && this.store.dispatch(new AddressRequested({ id: user.DeliveryAddressId }))),
    catchError((error) => {
      return of(error);
    }),
    map((userOrError: User | string) => {
      if(userOrError instanceof User) {
        return new UserLoaded({ user: userOrError });
      }
      return new UserLoadError({error: userOrError});
    }),
  );

  constructor(private actions$: Actions, private userService: UsersLocalService, private store: Store<AppState>) { }
}
