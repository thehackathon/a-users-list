import { AddressesState } from './../addresses/address.reducers';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UsersState } from './users.reducers';
import { User } from './models/user.model';
import { Address } from '../addresses/models/address.model';

export const selectUsersState = createFeatureSelector<UsersState>('users');
const selectAddressesState = createFeatureSelector<AddressesState>('addresses');

export const selectUserById = (id: number) => createSelector(
  selectUsersState,
  selectAddressesState,
  (usersState, addressState) => {
    const user = Object.assign(new User(), usersState.entities[id]);
    if(user && user.Id){
      user.DeliveryAddress = Object.assign(new Address(), addressState.entities[user.DeliveryAddressId]);
      return user;
    }
    return usersState.entities[id];
  }
);

export const selectError = () => createSelector(
  selectUsersState,
  usersState => usersState.error
);

export const selectUsersInStore = createSelector(
  selectUsersState,
  selectAddressesState,
  (usersState, addressState) => {
    const items: User[] = [];
    for(let key in usersState.entities) {
      let user = Object.assign(new User(), usersState.entities[key]);
      user.DeliveryAddress = Object.assign(new Address(), addressState.entities[user.DeliveryAddressId]);
      items.push(user);
    }
    return items;
  }
);
