import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {UsersActions, UsersActionTypes} from './users.actions';
import {User} from './models/user.model';

export interface UsersState extends EntityState<User> {
  error: string
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>({
  selectId: (user: User) => user.Id
});

export const initialUsersState: UsersState = adapter.getInitialState({error: null});

export function usersReducer(state = initialUsersState, action: UsersActions): UsersState {
  switch (action.type) {
    default: return state;
    case UsersActionTypes.UserLoaded: {
      return adapter.addMany([action.payload.user], {
        ...initialUsersState
      });
    }
    case UsersActionTypes.UsersGridListLoaded:
      return adapter.addMany(action.payload.users, {
        ...initialUsersState
      });
    case UsersActionTypes.UserLoadError:
      return {
        ...state,
        error: action.payload.error
      };
  }
}

const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();

export const selectAllUsersOnGrid = selectAll;
export const selectUserEntities = selectEntities;
export const selectUserIds = selectIds;
export const selectUsersTotalCount = selectTotal;
